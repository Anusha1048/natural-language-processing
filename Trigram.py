#!/bin/python3

import math
import os
import random
import re
import sys



if __name__ == '__main__':

    inputString = sys.stdin.read()

    inputString = inputString.lower()

    strings = [x.split() for x in inputString.split('.')]
    trigrams = []
    for row in strings:
        for i in range(len(row)-2):
            trigrams.append(row[i]+' '+row[i+1]+' '+row[i+2])
    count = []    
    for i in range(len(trigrams)):
        c = 0
        for j in range(len(trigrams)):
            if(trigrams[i] == trigrams[j]):
                c += 1
        count.append(c)
    pos = count.index(max(count))
    print(trigrams[pos].lower())
