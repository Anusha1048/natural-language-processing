import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy import spatial
import sklearn
import re
import numpy as np

REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS = set(['i',
 'me','my','myself','we','our','ours','ourselves','you','your','yours','yourself',
 'yourselves','he','him','his','himself','she','her','hers','herself','it','its',
 'itself','they','them','their','theirs','themselves','what','which','who','whom','this',
 'that','these','those','am','is','are','was','were','be','been','being','have',
 'has','had','having','do','does','did','doing','a','an','the','and','but','if','or',
 'because','as','until','while','of','at','by','for','with','about','against',
 'between','into','through','during','before','after','above','below','to','from',
 'up','down','in','out','on','off','over','under','again','further','then','once',
 'here','there','when','where','why','how','all','any','both','each','few','more','most',
 'other','some','such','no','nor','not','only','own','same','so','than','too','very',
 's','t','can','will','just','don','should','now'])

from nltk.stem.porter import PorterStemmer
porter = PorterStemmer()
import math

#print("starting")
def text_prepare(text):
    #text = text.split(". ")[0]
    text = text.lower()
    text = REPLACE_BY_SPACE_RE.sub(' ',text)
    text = BAD_SYMBOLS_RE.sub('',text)
    text = " ".join([x for x in text.split() if x not in STOPWORDS]) # delete stopwords from text
    text = porter.stem(text)
    return text

n=int(input())
a=[input() for _ in range(n)]
input()
b=[input() for _ in range(n)]

a=[text_prepare(x) for x in a]
b=[text_prepare(x) for x in b]
#print("Input loaded", len(a))

tfidf_vectorizer = TfidfVectorizer(ngram_range=(1,4), min_df=1, max_df=0.9, token_pattern='(\S+)')
tfidf_vectorizer.fit(a+b)
#print("vectorizer fit")
a=tfidf_vectorizer.transform(a)
b=tfidf_vectorizer.transform(b)
#print("vectorized")

distances=[[1]*n for i in range(n)]
candidatesA=[-1]*n
candidatesB=[-1]*n
remaining=set(range(n))

#print("start distances")
distances=sklearn.metrics.pairwise.cosine_distances(a,b)
#print("calculated")

for idx in range(n):
  #print(idx)
  (i,j)=np.unravel_index(distances.argmin(),  distances.shape)
  candidatesA[i]=j
  candidatesB[j]=i
  distances[i,:]=[2]*n
  distances[:,j]=[2]*n



for i in range(n):
    print(candidatesB[i]+1) 