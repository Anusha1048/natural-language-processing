import sys

def detect_lang():
    english_base = ['is ', 'are ', 'of ', 'the ']
    french_base = ['ne ', 'pas ', 'suis ', 'est ', 'sommes ', 'êtes ', 'sont ']
    german_base = ['bin ', 'bist ', 'ist ', 'sind ', 'seid ', 'und ', 'sie ']
    spanish_base = ['estoy ', 'estás ', 'está ', 'estamos ', 'estáis ', 'están ', 'soy ', 'eres ', 'somos ', 'sois ']

    english = 0
    french = 0
    german = 0
    spanish = 0

    while True:
        try:
            text_line = raw_input()
            # lowercase:
            text_line = text_line.lower()
            if 'ç' in text_line:
                return "French"
            if 'ê' in text_line:
                return "French"
            if 'ñ' in text_line:
                return "Spanish"
            if 'ü' in text_line:
                return "German"
            if 'ö' in text_line:
                return "German"
            if 'ä' in text_line:
                return "German"
        
            for word in english_base:
                if word in text_line:
                    english += 1
            for word in french_base:
                if word in text_line:
                    french += 1
            for word in german_base:
                if word in text_line:
                    german += 1
            for word in spanish_base:
                if word in text_line:
                    spanish += 1
        except:
            # in case of EOF we simply go out of the loop
            break
    languages = ['English', 'French', 'German', 'Spanish']
    count = [english, french, german, spanish]
    return languages[count.index(max(count))]

print detect_lang()
