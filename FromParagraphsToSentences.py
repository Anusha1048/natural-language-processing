# Enter your code here. Read input from STDIN. Print output to STDOUT
import os, sys
import re
def __matchHelper(openers, closers):
    """Return the matching close position for the first open position."""
    o = openers.pop(0)
    
    if len(openers) == 0:
        return closers.pop(0)
    
    c = closers[0]
    
    if c < openers[0]:
        c = closers.pop(0)
        __matchHelper(openers, closers)
    else:
        __matchHelper(openers, closers)
        c = closers.pop(0)
    
    if o >= c:
        raise IndexError("open >= close: %i >= %i" % (o, c))
    
    return c

def matchBracket(text, offset, limit=None):
    """Find the matching bracket to the one found at offset in text.
    
    The bracket at offset must be one of the six in '({[]})' - otherwise a
    RuntimeError is raised.
    
    Returns the offset of the other bracket or -1 if no closing bracket could
    be found.
    """
    brackets = (('(', ')'), ('[', ']'), ('{', '}'))
    opening, closing = None, None
    reverse = False
    
    
    for (o, c) in brackets:
        if text[offset] == o:
            opening, closing = o, c
            break
        elif text[offset] == c:
            opening, closing = o, c
            reverse = True
            break
    
    if opening is None:
        raise RuntimeError("character at %i not a bracket: '%s'" %
                           (offset, text[offset]))
    if reverse:
        end = offset + 1
        start = 0 if limit is None else offset - limit
    else:
        start = offset
        end = None if limit is None else offset + limit
    
    
    openers = offsets(text, opening, start=start, end=end)
    closers = offsets(text, closing, start=start, end=end)
    
    if len(openers) == 0 or len(closers) == 0:
        return -1
    
    if reverse:
        openers.reverse()
        closers.reverse()
        tmp = [i * -1 for i in openers]
        openers = [i * -1 for i in closers]
        closers = tmp
        
    try:
        matching_bracket = __matchHelper(openers, closers)
    except IndexError:
        return -1
    
    if reverse:
        return matching_bracket * -1
    
    return matching_bracket

def offsets(text, sub, start=0, end=None):
    """Return a list of offsets where sub is found in text.
    
    start and end can limit the search to a certain part of text."""
    if end is None:
        end = len(text)
    
    if start > end:
        raise IndexError("start > end: %i, %i" % (start, end))
    
    pos = text.find(sub, start, end)
    offsets = []
    
    while pos > -1 and pos < end:
        offsets.append(pos)
        pos = text.find(sub, pos + 1, end)
    
    return offsets


__dot_next = re.compile("([\.\?\!\:\;][\'\"]?[\s\n]+[\'\"]?[A-Z0-9])")
__newline = re.compile("[\n\s]+")

def simpleSplit(text):
    """Splits at any occasion of ([\.\?\!\:\;][\'\"]?[\s\n]+[\'\"]?[A-Z0-9])
    adding the punctuation mark to the last sentences and the first letter to
    the next - 'greedy' version; newlines are substituted to whitespaces.
    """
    tokens = __dot_next.split(__newline.sub(" ", text))
    sentences = [""]
    for idx in xrange(len(tokens)):
        if idx % 2:
            sentences[-1] += tokens[idx][0]
            sentences.append(tokens[idx][-1])
        else:
            sentences[-1] += tokens[idx]
    return sentences

# basic structure of a sentence end is *<punct> <start sentence>*
# and includes possible quotation marks
__terminals = re.compile(".*?[\.\!\?\:\;][\'\"]?[\s\n]+[\'\"]?[A-Z0-9]")
# basic structure of <start sentence>
__beginnings = re.compile("[\'\"]?[A-Z0-9]")
# basic structure of <abbrev>
__abbreviations = re.compile("[\w\-]+\.[\s\n]+")
# last abbreviation in a sentence is *<abbrev><punct> <start sentence>*
__final_abb = re.compile("[\w\-]+\.[\.\!\?\:\;][\'\"]?[\s\n]+[\'\"]?[A-Z0-9]")
# sentences ending in abbreviations must look like *<abbrev><punct> finally
__final_abb_test = re.compile(".*?\.[\.\!\?\:\;][\'\"]?[\s\n]+$")

def __abbrevs(text, start=0, limit=None):
    match = __abbreviations.match(text, start)
    end = start
    # slurp abbreviations
    while match is not None:
        end += len(match.group())
        match = __abbreviations.match(text, end)
    match = __final_abb.match(text, end)
    # ensure we arrived at the final abbreviation
    if match is not None:
        end += len(match.group()) - 1
        if match.group()[-2] in ("'", '"'): end -= 1
    # Jump over abbreviations within the sentence (ie. we have no beginning
    # after position end) -> a false alarm was triggered
    # NOTE: this is the reason why "Bla bla end. Abbrev. next sentence."
    # is not split by this system! This is more or less impossible to split
    # without deep analysis of the sentence(s).
    if not __beginnings.match(text, end): return __next(text, end, limit)
    # return whatever we slurped in additionally (or not)
    return end

def __brackets(text, start, end, limit):
    # try to find the last valid closing bracket (even beyond end) if there is
    # a  matching opening bracket within text[start:end] and return the
    # position of it
    result = next = start
    while next != -1:
       
        next = text.find("(", next, end)
        if next != -1:
            
            try:
                tmp = matchBracket(text, next, limit)
            except RuntimeError, msg:
                part = text
                try:
                    part = text[start:end]
                except IndexError, msg:
                    pass
                raise RuntimeError, \
                    "%s - start=%i, next=%i, end=%i, len=%i, txt='%s'" \
                    % (msg, start, next, end, len(text), part)
           
            if tmp > -1:
                result = next = tmp
            else:
                next = tmp
        
    return result

def __next(text, start=0, limit=None):
    match = __terminals.match(text, start)
    # no more terminals: return length of the text as end
    if match is None: return len(text)
    end = start + len(match.group()) - 1
    # see if we can jump over the current end because of brackets
    jump = __brackets(text, start, end, limit)
    # if so, recurse using the closing position of the bracktes as next start
    if jump > end: return __next(text, jump, limit)
    # move end by -1 if we matched quotation marks
    if match.group()[-2] in ("'", '"'): end -= 1
    # done if we are at a final abbreviation
    if __final_abb_test.match(text, start, end): return end
    # make sure any terminal abbreviations are slurped or we continue if this
    # was a false alarm
    return __abbrevs(text, end, limit)

def _sentences(text, start=0, limit=None):
    """Yield the index positions of the sentences in text."""
    length = len(text)
    end = __next(text, start, limit)
    while end != length:
        
        yield start, end
        start = end
        end = __next(text, start, limit)
    
    yield start, length

def split(text, limit=None):
    """More advanced version, which can handle strings like 'U. S. A.' or
    'Nat. Proc. Chem. Soc.' correctly (i.e. working 'non-greedy' by rather
    leaving a sentence joined than splitting it when it is not clear);
    newlines are substituted to whitespaces; finally, content in parenthesis
    is ignored and added to the sentence, as it quite often contains special 
    symbols and expressions those are more a hindrance than an advantage to 
    split up
    
    Set limit to a positive integer to define how far the algorithm should
    search for a closing bracket before it decides it is an unbalanced
    bracket. Good numbers are 250, 500 or 1000 characters.
    """
    #clean_text = __newline.sub(" ", text)
    clean_text = text
    return [clean_text[start:end].strip()
            for start, end in _sentences(clean_text, limit=limit)]

lines = sys.stdin.readlines()

if len(lines) > 1 or len(lines) == 0:
    sys.exit(-1)

input = lines[0]

sentenceEnders = re.compile(r'''([!?.]+)\s*(?=[A-Z])''')

results = []
sents = [line for line in split(input, limit=10000)]
#sents = [line + "\n" for line in simpleSplit(input)]

for line in sents:
    tls = sentenceEnders.split(line)
    if len(tls) > 2:
        #sys.stdout.writelines("\n".join(tls))
        n = len(tls)/2
        for i in range(len(tls)/2):
            i = i * 2
            #sys.stdout.write("".join(tls[i:i+2]) + "\n")
            results.append("".join(tls[i:i+2]) + "\n")
            results.append(tls[i+2] + "\n")
    else:
        results.append("".join(tls) + "\n")



sys.stdout.writelines(results)