import sys
import string
easy_fix = {
    'aint': "ain't", 'arent': "aren't", 'cant': "can't", 'couldve': "could've", 'couldnt': "couldn't", 'didnt': "didn't", 'doesnt': "doesn't", 'dont': "don't", 'hadnt': "hadn't", 'hed': "he'd", 'hell': "he'll", 'hes': "he's", 'id': "i'd",'ill': "i'll", 'im': "i'm", 'ive': "i've", 'isnt': "isn't", 'itd': "it'd", 'itll': "it'll", 'lets': "let's", 'mightnt': "mightn't", 'mustnt': "mustn't", 'mustve': "must've", 'oclock': "o'clock", 'shant': "shan't", 'shed': "she'd", 'shell': "she'll", 'shes': "she's", 'shouldve': "should've", 'thatll': "that'll", 'thats': "that's", 'therere': "there're", 'theres': "there's", 'theyll': "they'll",'wasnt': "wasn't", 'wed': "we'd", 'weve': "we've", 'whats': "what's", 'whod': "who'd", 'whore': "who're",'whos': "who's", 'wont': "won't", 'wouldve': "would've", 'wouldnt': "wouldn't", 'youll': "you'll", 'partys': "party's",
    'dos': "do's", 'donts': "don'ts", 'well': "we'll", 'were': "we're",
    'oxens': "oxen's", 'childrens': "children's", 'feets': "feet's", 'geeses': "geese's", 'louses': "louse's", 'mens': "men's", 'mouses': "mouse's", 'teeths': "teeth's", 'womens': "women's", 'peoples': "people's", 'indexs': "index's", 'matrixs': "matrix's", 'vertexs': "vertex's",
    'days': "days", 'always': 'always', 'theyve': "they've"
}
adjectives = []
verbs = ['been', 'born', 'beaten', 'become', 'begun', 'bitten', 'bled', 'blown', 'brought', 'broken', 'broadcast', 'built', 'burnt', 'bought', 'chosen', 'caught', 'come', 'cost', 'cut', 'done', 'drawn', 'dreamt', 'drunk', 'driven', 'eaten', 'fallen', 'felt', 'fed', 'fought', 'found', 'flown', 'forbidden', 'forgotten', 'frozen', 'got', 'given', 'gone', 'grown', 'hung', 'had', 'heard', 'hidden', 'hit', 'held', 'hurt', 'kept', 'known', 'laid', 'led', 'learnt', 'left', 'lend', 'let', 'lain', 'lost', 'lit', 'made', 'meant', 'met', 'paid', 'put', 'read', 'ridden', 'rung', 'risen', 'run', 'said', 'seen', 'sold', 'sent', 'set', 'shaken', 'shone', 'shot', 'shown', 'shut', 'sung', 'sat', 'slept', 'smelt', 'spoken', 'spelt', 'spent', 'spoilt', 'spread', 'stood', 'stolen', 'stuck', 'swept', 'swum', 'taken', 'taught', 'torn', 'told', 'thought', 'thrown', 'unterstood', 'woken', 'worn', 'won', 'written']

def is_suspicious(word):
    word = word.translate(string.maketrans("", ""), string.punctuation).lower()
    if word == '':
        return False

    if word[-1] == 's':
        if word in {'is', 'us', 'as', 'was', 'this', 'has', 'miss', 'years', 'his'}:
            return False
        if word[0] in {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}:
            return False
        return True

    return word in easy_fix

def get_strangeness(word):
    values_before, values_after, is_capital, i = '', '', False, 0
    while word[i] in string.punctuation:
        i += 1

    values_before, word = word[:i], word[i:]
    if word:
        i = len(word) - 1
        while word[i] in string.punctuation:
            i -= 1

        values_after, word = word[i + 1:], word[:i + 1]

    if word:
        is_capital = word[0].isupper()

    word = word.translate(string.maketrans("", ""), string.punctuation).lower()
    return word, {'before': values_before, 'after': values_after, 'is_cap': is_capital}

def apply_strangeness(word, strangeness):
    if strangeness['is_cap']:
        word = word.capitalize()

    return strangeness['before'] + word + strangeness['after']

def hardcoded_rules(word):
    if len(word) < 3 or word[-1] != 's':
        return False, ''

    if word[-3:] == 'sss':
        return True, word[:-1] + "'s"

    if word[-4:] in ['mans', 'mens']:
        return True, word[:-1] + "'s"

    if word[-6:] in ['womans', 'womens']:
        return True, word[:-1] + "'s"

    if word[-3:] == 'ies':
        return False, ''

    if word[-2:] == 'ys':
        return True, word[:-1] + "'s"

    if word[-3:] == 'oes':
        return False, ''

    if word[-3:] == 'ves':
        return False, ''

    if word[-2:] == 'fs':
        return True, word[:-1] + "'s"

    if word[-3:] == 'fes':
        return True, word[:-1] + "'s"

    return False, ''

def fix(word, next_word):
    (word, strangeness), fixed = get_strangeness(word), False
    if word in easy_fix:
        word, fixed = easy_fix[word], True
    elif word == 'its':
        word_next, strangeness_next = get_strangeness(next_word)
        next_possible = {
            'not', 'on', 'a', 'the', 'been', 'ready', 'very', 'good', 'bad', 'done', 'clear',
            'almost', 'always', 'only', 'just', 'who', 'more', 'your', 'too', 'because', 'okay',
            'important', 'an', 'all', 'wonderful', 'difficult', 'better', 'nice', 'every', 'from',
            'worth', 'about', 'hard'
        }
        if word_next in next_possible or word_next[-2:] == 'ed' or word_next[-3:] == 'ing' or word_next in adjectives or word_next in verbs:
            word, fixed = "it's", True

    else:
        fixed, word = hardcoded_rules(word)

    return fixed, apply_strangeness(word, strangeness)


def analyze(s):
    arr, output = s.split(), []
    for pos, val in enumerate(arr):
        if is_suspicious(val):
            is_fixed, fixed_word = fix(val, '' if pos == len(arr) - 1 else arr[pos + 1])
            if is_fixed:
                output.append(fixed_word)
            else:
                output.append(val)
            # print val, '\t', is_fixed, '\t', fixed_word if is_fixed else ''

        else:
            output.append(val)

    return ' '.join(output)


for line in sys.stdin:
    s = line.strip("\n")
    print analyze(s)