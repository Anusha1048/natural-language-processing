import math

def cos_dist(v, u):     #cosine dist between 2 vects
    uv = 0
    for i in range(0, len(v)):
        uv += v[i]*u[i]
    return uv**2

stopWords = ['', 'a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and', 'any', 'are', "aren't", 'as', 'at', 'be', 'because', 'been', 'before', 'being', 'below', 'between', 'both', 'but', 'by', "can't", 'cannot', 'could', "couldn't", 'did', "didn't", 'do', 'does', "doesn't", 'doing', "don't", 'down', 'during', 'each', 'few', 'for', 'from', 'further', 'had', "hadn't", 'has', "hasn't", 'have', "haven't", 'having', 'he', "he'd", "he'll", "he's", 'her', 'here', "here's", 'hers', 'herself', 'him', 'himself', 'his', 'how', "how's", 'i', "i'd", "i'll", "i'm", "i've", 'if', 'in', 'into', 'is', "isn't", 'it', "it's", 'its', 'itself', "let's", 'me', 'more', 'most', "mustn't", 'my', 'myself', 'no', 'nor', 'not', 'of', 'off', 'on', 'once', 'only', 'or', 'other', 'ought', 'our', 'ours\tourselves', 'out', 'over', 'own', 'same', "shan't", 'she', "she'd", "she'll", "she's", 'should', "shouldn't", 'so', 'some', 'such', 'than', 'that', "that's", 'the', 'their', 'theirs', 'them', 'themselves', 'then', 'there', "there's", 'these', 'they', "they'd", "they'll", "they're", "they've", 'this', 'those', 'through', 'to', 'too', 'under', 'until', 'up', 'very', 'was', "wasn't", 'we', "we'd", "we'll", "we're", "we've", 'were', "weren't", 'what', "what's", 'when', "when's", 'where', "where's", 'which', 'while', 'who', "who's", 'whom', 'why', "why's", 'with', "won't", 'would', "wouldn't", 'you', "you'd", "you'll", "you're", "you've", 'your', 'yours', 'yourself', 'yourselves']
    
no = int(input())
A = []
B = []
words = []
pre = []
d = []
N = 2*no
for i in range(0, no):          #splits first half of text into words
    block = input().split()
    text = []
    for i, word in enumerate(block):
        if block[i-1][-1] == '.' and  "A" <= word[0] <= "Z":
            word = word.lower()
        word = word.strip('.').strip(',').strip('"').strip(')')
        if not word in words:
            if not word in pre:
                pre.append(word)
            else:
                words.append(word)
                d.append(0)
        text.append(word)
    for i in range(0, len(words)):
        if words[i] in text:
            d[i] += 1
    A.append(text)  

input()

for i in range(0, no):
    block = input().split()
    text = []
    for i, word in enumerate(block):           
        if block[i-1][-1] == '.' and  "A" <= word[0] <= "Z":
            word = word.lower()
        word = word.strip('.').strip(',').strip('"').strip(')')
        if (not word in words) and (not word in stopWords):
            if not word in pre:
                pre.append(word)
            else:
                words.append(word)
                d.append(0)
        text.append(word)
    for i in range(0, len(words)):
        if words[i] in text:
            d[i] += 1
    B.append(text)    

    
A_vectors = []
for text in A:
    l = len(text)
    vector = [(text.count(words[i]))*(N/d[i]) for i in range(0, len(words))]
    A_vectors.append(vector)
    
B_vectors = []
for text in B:
    l = len(text)
    vector = [(text.count(words[i]))*(N/d[i]) for i in range(0, len(words))]
    B_vectors.append(vector)

A_mag = [sum(x**2 for x in A_vec) for A_vec in A_vectors]
B_mag = [sum(x**2 for x in B_vec) for B_vec in B_vectors]

distances = []
for n in range(0, no):
    distance = [cos_dist(B_vectors[i], A_vectors[n])/(A_mag[n]*B_mag[i]) for i in range(0, no)]
    distances.append(distance)     

A_unpaired = []
closest = [0]*no
for i in range(0, no):
    B_index = distances[i].index(max(distances[i]))
    if distances[i][B_index] == max(distances[j][B_index] for j in range(0, no)):
        closest[i] = B_index + 1
    

while 0 in closest:
    B_unpaired = []
    A_unpaired = []
    for i in range(1,no+1):
        if i not in closest:
            B_unpaired.append(i-1)
        if closest[i-1] == 0:
            A_unpaired.append(i-1)

    for i in A_unpaired:
        B_index = distances[i].index(max(distances[i][j] for j in B_unpaired))
        if distances[i][B_index] == max(distances[j][B_index] for j in A_unpaired):
            closest[i] = B_index + 1

        
for el in closest:
    print(el)