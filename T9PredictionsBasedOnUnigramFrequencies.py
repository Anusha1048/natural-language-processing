import re
from itertools import product
from operator import itemgetter

unigrams = {}

# populating dictionary
with open('t9Dictionary') as f:
    for _ in range(int(f.readline())):
        line = f.readline().strip()
        unigrams[line] = 1

# updating unigram frequencies using corpus
with open('t9TextCorpus') as f:
    while True:
        line = f.readline().strip()
        if line == "END-OF-CORPUS":
            break
        for m in re.findall(r"\b[-'A-Za-z]+\b", line):
            m = m.lower()
            if m in unigrams:
                unigrams[m] += 1


def getLetters(digit):
    return {
        '0': ['0'],
        '1': ['0'],
        '2': ['a', 'b', 'c'],
        '3': ['d', 'e', 'f'],
        '4': ['g', 'h', 'i'],
        '5': ['j', 'k', 'l'],
        '6': ['m', 'n', 'o'],
        '7': ['p', 'q', 'r', 's'],
        '8': ['t', 'u', 'v'],
        '9': ['w', 'x', 'y', 'z'],
    }[digit]

sdic = list(sorted(unigrams.items()))

for _ in range(int(input())):
    entry = list(input())
    words = []
    for i in range(len(entry)):
        words.append(getLetters(entry[i]))
    ps = []
    for p in product(*words):
        ps.append("".join(p))
    i = 0
    wi = 0
    candidates = []
    while True:
        if wi == len(sdic):
            break
        word = sdic[wi]
        if word[0] < ps[i]:
            wi += 1
        elif word[0] == ps[i] or word[0].startswith(ps[i]):
            candidates.append(word)
            wi += 1
        else:
            i += 1
            if i == len(ps):
                break
    # retrieving top 5
    bms = sorted(candidates,key=itemgetter(1),reverse=True)[:5]
    if len(bms)==0:
        print("No Suggestions")
    else:
        print(";".join([b[0] for b in bms]))
