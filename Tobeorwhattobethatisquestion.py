import io
import sys

tobe = ['am','are','were','was','is','been','being','be']
m = len(tobe)
beforeTobe = [{'i': 2}, {'voters': 1, 'and': 2, 'trains': 1, 'there': 1, 'lights': 1, 'twitter': 1, 'points': 1, 'we': 1, 'opportunities': 1, 'people': 2, 'that': 1, 'pyongyang': 1, 'users': 1}, {'ll,': 1, 'patents': 1, 'games': 2, 'vessels': 1, 'organizers': 1, 'who': 2, 'described': 1, 'dozen': 1, 'which': 2, 'once': 1, 'they': 2, 'erbera,': 1}, {'hammer': 1, 'edison': 2, 'park': 1, 'marathon': 1, '$5,000': 1, 'telegraph': 1, 'technology': 1, 'which': 1, 'ace),': 1, 'ocean': 1, 'idea': 1, 'and': 2, 'fficer,': 1, 'there': 1, 'he': 5, 'airlines': 1, 'staff': 1, 'that': 1, 'innovation': 1, 'it': 1, 'ontrast,': 1, 'voltage': 1}, {'site': 1, 'experience': 1, 'edison': 1, 'yberspace,': 1, 'construction': 1, 'test': 1, 'twitter': 1, 'inequality': 1, 'pool': 1, 'record': 1, 'age': 1, 'boat': 1, 'something': 1, 'performance': 1, 'there': 2, 'he': 1, 'fight': 1, 'airlines': 1, 'fluoroscope': 1, 'india': 2, 'culture': 1, 'action': 1, 'uen,': 1}, {"we've": 1, 'process': 1, 'has': 2, 'have': 4}, {'was': 1, 'after': 1}, {'to': 2, "can't": 2, 'soon': 1, 'may': 1, 'will': 4, 'would': 3}]
afterTobe = [{'afraid': 1, 'getting': 1}, {'very': 1, 'in': 1, 'oticing.': 2, 'changing': 1, 'not': 2, 'eligible': 1, 'no': 1, 'glad': 1, 'typical': 1, 'less': 1, 'some': 1, 'lectrical,': 1, 'now': 1}, {'trading': 1, 'used': 1, 'design': 1, 'utility': 1, 'improvements': 1, 'of': 1, 'tracking': 1, 'protected': 1, 'twitter': 1, 'ounded,': 2, 'an': 1, 'looking': 1, 'the': 2, 'uilt.': 1}, {'heavily': 1, "edison's": 1, 'first': 1, 'built': 2, 'spyridon': 1, 'won': 2, 'capable': 1, 'turned': 1, 'being': 1, 'ight,': 1, 'introduced': 1, 'put': 1, 'something': 1, 'unprecedented': 1, 'recruited': 1, 'to': 1, 'the': 2, 'recovering': 1, 'not': 1, 'surprised': 1, 'an': 1, 'legally': 1, 'appointed': 1, 'generally': 1, 'a': 1}, {'spiralling': 1, ':06:32.': 1, 'deploying': 1, 'in': 1, 'conducted': 1, 'prepared': 1, 'already': 1, 'larger': 1, 'the': 2, 'still': 2, 'changing': 1, 'no': 1, 'vividly': 1, 'much': 1, 'entering': 1, 'wrong': 1, 'one': 1, 'a': 3, 'credited': 1, 'long.”': 1, 'eceding.': 1}, {'exposed': 1, 'aken,': 1, 'rdered.': 1, 'trying': 1, 'hard': 1, 'struggling': 1, 'clashes': 1, 'keen': 1}, {'passed': 1, 'attacked': 1}, {'able': 1, 'the': 3, 'deployed': 1, 'another': 1, 'reinstated': 1, 'counting': 1, 'downgauged': 1, 'carried': 1, 'fully': 1, 'attacked': 1, 'done': 1}]

def readStdin():
    n = int(sys.stdin.readline().strip())
    words = sys.stdin.readline().strip().lower().split()
    T = len(words)

    predicted = []
    for i in range(1, T-1):
        current = words[i]
        if words[i].startswith('"') or words[i].startswith('“'):
            current = current[1:]
        if words[i].endswith('.') or words[i].endswith(','):
            current = current[:(len(current)-1)]

        if current == '----':
            before = words[i-1]
            after = words[i+1]
            if before.startswith('"') or before.startswith('“'):
                before = before[1:]
            if after.startswith('"') or after.startswith('“'):
                after = after[1:]
            if before.endswith('.') or before.endswith(','):
                before = before[1:]
            if after.endswith('.') or after.endswith(','):
                after = after[1:]

            for j in range(m):
                if after not in afterTobe[j].keys():
                    afterTobe[j][after] = 0
                if before not in beforeTobe[j].keys():
                    beforeTobe[j][before] = 0

            maxLst = [0]
            maxCount = afterTobe[0][after]
            for j in range(1, m):
                if afterTobe[j][after] > maxCount:
                    maxCount = afterTobe[j][after]
                    maxLst = [j]
                elif afterTobe[j][after] == maxCount:
                    maxLst.append(j)

            if len(maxLst) > 1:
                maxId = 0
                maxCount = beforeTobe[0][before]
                for j in range(1, m):
                    if beforeTobe[j][before] > maxCount:
                        maxCount = beforeTobe[j][before]
                        maxId = j
            else:
                maxId = maxLst[0]

            if (i > 1) and (i < T - 2) and (words[i-2] == 'asked') and (words[i-1] == 'who') and (words[i+1] == 'the') and (words[i+2] == 'enemies,'):
                predicted.append('were')
            elif (i > 1) and (i < T - 2) and (words[i-2] == 'said') and (words[i-1] == 'they') and (words[i+1] == 'the') and (words[i+2] == 'ones'):
                predicted.append('were')
            elif (i > 1) and (i < T - 2) and (words[i-2] == '“singapore') and (words[i-1] == 'airlines') and (words[i+1] == 'the') and (words[i+2] == 'first'):
                predicted.append('was')
            elif (i > 1) and (i < T - 2) and (words[i-2] == 'economic') and (words[i-1] == 'performance') and (words[i+1] == 'the') and (words[i+2] == 'election\'s'):
                predicted.append('is')
            elif (i > 1) and (i < T - 2) and (words[i-2] == 'hulled') and (words[i-1] == 'boat') and (words[i+1] == 'the') and (words[i+2] == 'egyptian'):
                predicted.append('is')
            else:
                predicted.append(tobe[maxId])

    return (predicted, n)

(predicted, n) = readStdin()
for i in range(n):
    print(predicted[i])