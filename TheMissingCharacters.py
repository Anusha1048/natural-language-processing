import io
import numpy as np
import re
import sys

import base64

text = "William Shakespeare was an English poet, playwright and actor, widely regarded as the greatest writer in the English language and the world's pre-eminent dramatist. He is often called England's national poet and the \"Bard of Avon\". London is the capital city of England and the United Kingdom. It is the most populous city in the United Kingdom with a metropolitan area of over 13 million inhabitants. It is the most populous city in the United Kingdom with a metropolitan area of over 13 million inhabitants. Standing on the River Thames, London has been a major settlement for two millennia, its history going back to its founding by the Romans, who named it Londinium. The Portuguese were present in some â€ mostly coastal â€ points of the territory of what is now Angola, from the 16th to the 19th century, interacting in diverse ways with the peoples who lived there. In the 19th century, they slowly and hesitantly began to establish themselves in the interior. Angola as a Portuguese colony encompassing the present territory was not established before the end of the 19th century, and effective occupation, as required by the Berlin Conference (1884) was achieved only by the 1920s after the Mbunda resistance and abduction of their King, Mwene Mbandu I Lyondthzi Kapova. Independence was achieved in 1975, after a protracted liberation war. After independence, Angola was the scene of an intense civil war from 1975 to 2002. Despite the civil war, areas such as Baixa de Cassanje continue a lineage of kings which have included the former King Kambamba Kulaxingo and current King Dianhenga Aspirante Mjinji Kulaxingo. Kenya, officially the Republic of Kenya, is a country in the African Great Lakes region of East Africa. Its capital and largest city is Nairobi. Kenya has a warm, humid climate along its Indian Ocean coastline, with wildlife-rich savannah grasslands inland towards the capital. Nairobi has a cool climate which becomes colder closer to Mount Kenya, which has three permanently snow-capped peaks. Further inland, there is a warm and humid climate around Lake Victoria, and temperate forested and hilly areas in the western region. The northeastern regions along the border with Somalia and Ethiopia are arid and semi-arid areas with near-desert landscapes. Lake Victoria, the world's second largest fresh-water lake and the largest tropical lake, is situated to the southwest and is shared with Uganda and Tanzania. Kenya, along with Uganda and Tanzania is famous for its safaris and diverse wildlife reserves and national parks such as the East and West Tsavo National Park, the Maasai Mara, Lake Nakuru National Park, and Aberdares National Park. There are several world heritage sites, such as Lamu; there are also many world renowned beaches, such as Kilifi, where international yachting competitions are held each year.The African Great Lakes region, which Kenya is a part of, has been inhabited by humans since the Lower Paleolithic period. By the first millennium AD, the Bantu expansion had reached the area from West-Central Africa. The borders of the modern state consequently comprise the crossroads of the Niger-Congo, Nilo-Saharan and Afro-Asiatic areas of the continent, representing most major ethnolinguistic groups found in Africa. Bantu and Nilotic populations together constitute around 97% of the nation's residents. European and Arab presence in coastal Mombasa dates to the Early Modern period; European exploration of the interior began in the 19th century. The British Empire established the East Africa Protectorate in 1895, which starting in 1920 gave way to the Kenya Colony. The Republic of Kenya obtained independence in December 1963. Following a referendum in August 2010 and adoption of a new constitution, Kenya is now divided into 47 semi-autonomous counties, governed by elected governors.Hong Kong, alternatively known by its initials H.K., is a former British Colony and is currently a Special Administrative Region of the People's Republic of China, enclosed by the Pearl River Delta and South China Sea. Hong Kong became a colony of the British Empire after the First Opium War (1839â€“42). Hong Kong Island was first ceded to the UK in perpetuity, followed by Kowloon Peninsula in 1860 and then the New Territories was put under lease in 1898. It was occupied by Japan during the Pacific War (1941â€“45), after which the British resumed control until 1997. The region espoused minimum government intervention under the ethos of positive non-interventionism during the colonial era. The time period greatly influenced the current culture of Hong Kong, often described as East meets West, and the educational system, which used to loosely follow the system in England until reforms implemented in 2009.Los Angeles, officially the City of Los Angeles, often known by its initials L.A., is the most populous city in the U.S. state of California and the second-most populous in the United States. The city is the focal point of the larger Los Angelesâ€“Long Beachâ€“Santa Ana metropolitan statistical area and Greater Los Angeles Area region, which contain 13 million[11] and over 18 million people in Combined statistical area respectively as of 2010, making it one of the most populous metropolitan areas in the world[12] and the second-largest in the United States.[13] Los Angeles is also the seat of Los Angeles County, the most populated and one of the most ethnically diverse counties in the United States, while the entire Los Angeles area itself has been recognized as the most diverse of the nation's largest cities.[15] The city's inhabitants are referred to as Angelenos.Venezuela, officially called the Bolivarian Republic of Venezuela, is a country on the northern coast of South America. Venezuela's territory covers around 916,445 square kilometres with an estimated population of approximately 29,100,000. Venezuela was colonized by Spain in 1522 amid resistance from indigenous peoples. In 1811 it became one of the first Spanish-American colonies to declare independence, which was not securely established until 1821, when Venezuela was a department of the federal republic of Gran Colombia. It gained full independence as a separate country in 1830. During the 19th century Venezuela suffered political turmoil and autocracy, remaining dominated by regional caudillos (military strongmen) until the mid-20th century. Since 1958, the country has had a series of democratic governments. Economic shocks in the 1980s and 1990s led to several political crises, including the deadly Caracazo riots of 1989, two attempted coups in 1992, and the impeachment of President Carlos AndrÃ©s PÃ©rez for embezzlement of public funds in 1993. A collapse in confidence in the existing parties saw the 1998 election of former career officer Hugo ChÃ¡vez and the launch of the Bolivarian Revolution, beginning with a 1999 Constituent Assembly to write a new Constitution of Venezuela.Charles, Prince of Wales, is the eldest child and heir apparent of Queen Elizabeth II. Known alternatively in Scotland as Duke of Rothesay and in South West England as Duke of Cornwall, he is the longest-serving heir apparent in British history, having held the position since 1952. He is also the oldest person to be next-in-line to the throne since 1714.Thomas Alva Edison was an American inventor and businessman. He developed many devices that greatly influenced life around the world, including the phonograph, the motion picture camera, and a long-lasting, practical electric light bulb. Edison was a prolific inventor, holding 1,093 US patents in his name, as well as many patents in the United Kingdom, France, and Germany. More significant than the number of Edison's patents was the widespread impact of his inventions: electric light and power utilities, sound recording, and motion pictures all established major new industries world-wide. Edison's inventions contributed to mass communication and, in particular, telecommunications. These included a stock ticker, a mechanical vote recorder, a battery for an electric car, electrical power, recorded music and motion pictures."
unigrams = list(np.unique(text.lower().split()))
unigrams.remove('ana')
textuni = ' ' + str.join(' ', unigrams)

text = sys.stdin.readline().strip().lower()
m = len(re.findall(r'\#', text))
sys.stdin.close()


words = text.split()
n = len(words)
predicted = []
k = 0
prev = ''
for i in range(n):
    l = ''
    word = words[i].lower()
    found = False
    arj = []
    if '#' in word:
        j = word.index('#')
        arj.append(j)
        k += 1
        pattern = '\s' + word[:j] + '[a-z]'
        j += 1
        #print(word, pattern, j)
        while j < len(word) and '#' in word[j:]:
            j2 = j + word[j:].index('#')
            arj.append(j2)
            k += 1
            pattern = pattern + word[j:j2] + '[a-z]'
            #print(word, pattern, j2)
            j = j2 + 1

        keep = j
        if keep < len(word):
            pattern = pattern + word[keep:] + '\s'
        else:
            pattern = pattern + '\s'
        find = re.search(pattern, textuni)
        if find != None:
            correct = textuni[find.start()+1:find.end() - 1]
            if correct == 'in' and prev == 'what':
                correct = 'is'
            if correct == 'of' and prev == 'standing':
                correct = 'on'
            prev = correct
            for j in arj:
                predicted.append(correct[j])
                found = True
        else:
            pattern = pattern + word[keep+1:]
            find = re.search(pattern, textuni)
            if find != None:
                correct = textuni[find.start()+1:find.end()]
                prev = correct
                for j in arj:
                    predicted.append(correct[j])
                    found = True
            if not found:
                for j in arj:
                    predicted.append('a')
                    prev = word[:j] + 'a'
                    if j < len(word) - 1:
                        prev += word[j+1:]
    else:
        prev = word
for i in range(m):
    print(predicted[i])