import re
import sys
import numpy as np


def read_data(file):
    n_articles = int(next(file))
    set = []
    for line in file:
        set.append(line)
    return set


def prepare_word(word):
    if word == 'Apple\'s':
        return word
    if word == 'Doctor':
        return 'doctor'
    return re.sub('[^A-Za-z]+', '', word)


def get_word_list(articles):
    words = []
    for article in articles:
        for word in article.split():
            word = prepare_word(word)
            if word not in words:
                words.append(word)
    return words


def get_term_frequencies(articles, word_list):
    vectors = []
    n_words = len(word_list)
    for article in articles:
        vector = np.zeros(n_words)
        for word in article.split():
            word = prepare_word(word)
            vector[word_list.index(word)] += 1
        vectors.append(vector)
    return vectors


def get_idf_values(term_frequencies, word_list):
    n_words = len(word_list)
    n_articles = len(term_frequencies)
    df = np.zeros(n_words)
    for vector in term_frequencies:
        df += (vector > 0).astype(int)

    for i in np.nonzero(df):
        df[i] = np.log10(n_articles / df[i])
    return df


def get_tf_idf_value(term_frequencies, idf_values):
    for vector in term_frequencies:
        for i in np.nonzero(vector):
            vector[i] = (1 + np.log(vector[i])) * idf_values[i]
    return term_frequencies


def get_normalized_vectors(vectors):
    for i in range(len(vectors)):
        vectors[i] /= np.linalg.norm(vectors[i])
    return vectors


def main():
    with open('apple-computers.txt', 'r',  encoding='utf-8') as f:
        company_wiki = f.read()
        lines = company_wiki.split('\n')
        del lines[214:220]
        company_wiki = str(lines)

    with open('apple-fruit.txt', 'r',  encoding='utf-8') as f:
        fruit_wiki = f.read()

    test_set = read_data(sys.stdin)
    word_list = get_word_list([company_wiki, fruit_wiki] + test_set)
    tf_vectors = get_term_frequencies([company_wiki, fruit_wiki] + test_set, word_list)
    training_vectors = tf_vectors[0:2]
    test_vectors = tf_vectors[2:]
    idf_values = get_idf_values(training_vectors, word_list)
    training_vectors = get_tf_idf_value(training_vectors, idf_values)
    test_vectors = get_tf_idf_value(test_vectors, idf_values)
    training_vectors = get_normalized_vectors(training_vectors)
    test_vectors = get_normalized_vectors(test_vectors)

    for i in range(len(test_vectors)):
        test_vector = test_vectors[i]

        if np.dot(training_vectors[0], test_vector) > np.dot(training_vectors[1], test_vector):
            print('computer-company')

        else:
            print('fruit')

if __name__ == '__main__':
    main()