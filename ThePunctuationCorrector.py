import re

n = int(input())

for _ in range(n):
    s = input()
    s = re.sub(r"(\,\s)\?\?\?", r"\1it's", s)
    if s[:3] == "???":
        s = re.sub(r"^\?\?\?", r"It's", s)
        s = re.sub(r"\?\?\?", r"it's", s)
        print(s)
    else: 
        print(re.sub(r"\?\?\?", r"its", s))
    