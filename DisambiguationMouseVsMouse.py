from random import randint


def check_word_list(sentence, word_list):
    #convert to lowercase
    s = sentence.lower()
    n_words = len(word_list)
    for i in range(n_words):
        if word_list[i] in s:
            return True
    return False


biol_mice = ["genome", "genomes", "natal", "food", "tail", "ear", "whiskers", "rat", "house", "cat", "mickey", "trap", "cheese", "sqeak", "nibble", "gnaw", "infest", "lab", "rodent", "neural", "neuro", "experiment", "gene", "science", "litter", "health", "monitor", "sex", "male", "nest", "cage", "water", "sanitation"]

#"research", "promot", "modif", "sequence"]

#don't really help
#"dye", "fluor", "fed", "feed", "mod"
#"attic", "poison", "hay", "cellar", "basement",
#"scratch", "dog", "house", "scare"

computer_mouse = ["device", "cable", "button", "cord", "input", "wire", "optical", "sensor", "hand", "click", "digital", "usb", "wheel", "office", "computer", "game", "work", "pad", "manufact", "left", "keyboard", "pc", "screen", "press", "scroll", "drag", "zoom", "strain", "surface", "portable", "elbow", "practice", "grip", "hold"] 
                  

#read input data
N = int(input())

for i in range(N):
    #read next sentence
    sent = input()
    
    #check if one of the mouse words occurs
    if check_word_list(sent, biol_mice):
        print("animal")
    elif check_word_list(sent, computer_mouse):
        print("computer-mouse")
    else:
        #print("Don't know")
        #random decision
        if randint(0,1)>0:
            print("animal")
        else:
            print("computer-mouse")
    
    #if check_word_list(sent, computer_mouse):
    #    print("computer-mouse")
    #else:
    #    print("animal")
        
    #if check_word_list(sent, biol_mice):
    #    print("animal")
    #else:
    #    print("computer-mouse")        

         
        